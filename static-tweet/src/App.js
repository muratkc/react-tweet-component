import React, { Component } from 'react';
import './App.css';

var testTweet = {
  message: "Something about cats.",
  gravatar: "xyz",
  author: {
    handle: "catperson",
    name: "IAMA Cat Person"
  },
  likes: 2,
  retweets: 0,
  timestamp: "2016-07-30 21:24:37"
};

class App extends Component {
  render() {
    return (
      <Tweet tweet={testTweet}/>
    );
  }
}

class Tweet extends Component {
  render() {
    return(
      <div className="tweet">
        <Avatar hash={this.props.tweet.gravatar}/>
        <div className="content">
          <NameWithHandle /><Time />
          <Message text={this.props.tweet.message}/>
          <div className="buttons">
            <ReplyButton />
            <RetweetButton />
            <LikeButton />
            <MoreOptionsButton />
          </div>
        </div>
      </div>
    );
  }
} 

class Avatar extends Component {
  render() {
    var hash = this.props.hash;
    var url = `https://www.gravatar.com/avatar/${hash}`;
    return(
      <img src={url}
          className="avatar"
          alt="avatar" />
    );
  }
}

class Message extends Component {
  render() {
    return(
      <div className="message">
        {this.props.text}
      </div>
    );
  }
}

var NameWithHandle = React.createClass({
  render: function() {
    return(
      <span className="name-with-handle">
        <span className="name">Name</span>
        <span className="handle">@handle</span>
      </span>
    );
  }
});

var Time = React.createClass({
  render: function() {
    return(
      <span className="time">3h ago</span>
    );
  }
});

class ReplyButton extends Component {
  render() {
    return(
      <i className="fa fa-reply reply-button" />
    );
  }
}

class RetweetButton extends Component {
  render() {
    return(
      <i className="fa fa-retweet retweet-button" />
    );
  }
}

class LikeButton extends Component {
  render() {
    return(
      <i className="fa fa-heart like-button" />
    );
  }
}

class MoreOptionsButton extends Component {
  render() {
    return(
      <i className="fa fa-ellipsis-h more-options-button" />
    );
  }
}

export default App;
