import React, { Component } from 'react';
import './App.css';
import moment from 'moment';

var testTweet = {
  message: "Ceci est un message",
  gravatar: "xyz",
  author: {
    handle: "muratk",
    name: "Murat"
  },
  likes: 2,
  retweets: 1,
  timestamp: "2016-07-30 21:24:37"
};

class App extends Component {
  render() {
    return (
      <Tweet tweet={testTweet}/>
    );
  }
}

class Tweet extends Component {
  render() {
    var {tweet} = this.props;
    return(
      <div className="tweet">
        <Avatar hash={tweet.gravatar}/>
        <div className="content">
          <NameWithHandle author={tweet.author}/>
          <Time time={tweet.timestamp}/>
          <Message text={tweet.message}/>
          <div className="buttons">
            <ReplyButton />
            <RetweetButton count={tweet.retweets}/>
            <LikeButton count={tweet.likes}/>
            <MoreOptionsButton />
          </div>
        </div>
      </div>
    );
  }
} 

class Avatar extends Component {
  render() {
    var hash = this.props.hash;
    var url = `https://www.gravatar.com/avatar/${hash}`;
    return(
      <img src={url}
          className="avatar"
          alt="avatar" />
    );
  }
}
Avatar.propTypes = {
  hash: React.PropTypes.string.isRequired
}

class Message extends Component {
  render() {
    return(
      <div className="message">
        {this.props.text}
      </div>
    );
  }
}
Message.propTypes = {
  text: React.PropTypes.string.isRequired
}

var NameWithHandle = React.createClass({
  propTypes: {
    author: React.PropTypes.shape({
      name: React.PropTypes.string.isRequired,
      handle: React.PropTypes.string.isRequired
    }).isRequired
  },
  render: function() {
    var {name, handle} = this.props.author;
    return(
      <span className="name-with-handle">
        <span className="name">{name}</span>
        <span className="handle">@{handle}</span>
      </span>
    );
  }
});

var Time = React.createClass({
  propTypes: {
    time: React.PropTypes.string.isRequired
  },
  computeTimeString: function() {
    return moment(this.props.time).fromNow();
  },
  render: function() {
    return(
      <span className="time">{this.computeTimeString()}</span>
    );
  }
});

class ReplyButton extends Component {
  render() {
    return(
      <i className="fa fa-reply reply-button" />
    );
  }
}

class RetweetButton extends Component {
  getCount() {
    if(this.props.count > 0){
      return (
        <span className="retween-count">
          {this.props.count}
        </span>
      );
    } else{
      return null;
    }
  }
  render() {
    return(
      <span className="retweet-button">
        <i className="fa fa-retweet" />
        {this.getCount()}
      </span>
    );
  }
}
RetweetButton.propTypes = {
  count: React.PropTypes.number
}

class LikeButton extends Component {
  render() {
    return(
      <span className="like-button">
        <i className="fa fa-heart" />
        {this.props.count > 0 ?
          <span className="like-count">{this.props.count}</span>
          : null}
      </span>
    );
  }
}
LikeButton.propTypes = {
  count: React.PropTypes.number
}

class MoreOptionsButton extends Component {
  render() {
    return(
      <i className="fa fa-ellipsis-h more-options-button" />
    );
  }
}

/*Tweet.propTypes = {
  tweet: React.PropTypes.object
}*/
Tweet.PropTypes = {
  tweet: React.PropTypes.shape({
    avatar: React.PropTypes.element.isRequired,
    author: React.PropTypes.object.isRequired,
    time: React.PropTypes.element.isRequired,
    message: React.PropTypes.element.isRequired,
    retweets: RetweetButton.PropTypes.element.isRequired,
    likes: LikeButton.PropTypes.element.isRequired
  })  
}


export default App;
